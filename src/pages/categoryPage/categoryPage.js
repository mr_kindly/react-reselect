import React from 'react';
import { connect } from 'react-redux';
import classnames from 'classnames';
import { createSelector } from 'reselect';
import { fetchCategoryProducts } from './../../store/actions/product';
import ProductGrid from '../../components/productGrid';


const getProducts = products => products;

const getTotalSize = createSelector(
  getProducts,
  products => (console.log('calculate'), products.length)
);

class CategoryPage extends React.PureComponent {
  state = {
    sortBy: 'name'
  };

  componentDidMount() {
    this.getCategoryData();
  }

  componentWillReceiveProps(nextProps) {
    const { match: { params: nextParams } } = nextProps;
    const { match: { params: currentParams } } = this.props;
    if (nextParams.id !== currentParams.id) {
      this.getCategoryData(nextProps);
    }
  }

  getCategoryData(props = this.props) {
    const { match: { params } } = props;
    const { id } = params;

    if (id) {
      this.props.fetchCategoryProducts(id);
    }
  }

  handleSort(sortBy) {
    this.state.sortBy = sortBy;
    this.forceUpdate();
  }
  render() {
    const { products } = this.props;
    return (!products ? <h2>Product's list is empty</h2>
      : <div>
          <div>
            <h4>Total: {getTotalSize(products)}</h4>
            <div className="btn-group" role="group">
              <div
                className={classnames("btn btn-default", {"btn-primary": this.state.sortBy === 'name'})}
                onClick={() => this.handleSort('name')}
              >By name</div>
              <div
                className={classnames("btn btn-default", {"btn-primary": this.state.sortBy === 'price'})}
                onClick={() => this.handleSort('price')}
              >By price</div>
            </div>
          </div>
        <ProductGrid products={products} />
      </div>);
  }
}


const mapStateToProps = ({ category }) => {
  const { products } = category;
  return {
    products,
  };
};

export default connect(
  mapStateToProps,
  {
    fetchCategoryProducts,
  },
)(CategoryPage);
